var clicked = 0;

document.getElementById("burger-button").addEventListener("click", function(){

    clicked = !clicked;
    if(clicked) {
        document.getElementById("burger-button").classList.add("header-nav-show");
        document.getElementById("navbar").classList.add("header-nav-show");
    } else {
        document.getElementById("burger-button").classList.remove("header-nav-show");
        document.getElementById("navbar").classList.remove("header-nav-show");

    }
})