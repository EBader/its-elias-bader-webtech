function fetchNews() {
    var newsContainer = document.getElementById("news-content");
    
    fetch("./data/news.json")
    .then((response) => {
      return response.json();
    })
    .then((data) => {
        for(let i = 0; i < data["news"].length; i++) {
            newsContainer.innerHTML += "<div class='aside-item'><p class='searchable'>" + data["news"][i]["info"] + "</p></div>";
        }
    });
}


function fetchArticles() {
    var mainContainer = document.getElementById("main-content");
    var articleContainer = document.getElementById("article-content");
    var articleBigContainer = document.getElementById("article-big-content");
    
    fetch("./data/articles.json")
    .then((response) => {
      return response.json();
    })
    .then((data) => {

        for(let i = 0; i < data["article"].length; i++) {
            if(data["article"][i]["type"] === "main") {
                mainContainer.innerHTML += "<img class='main-img' src='" + data["article"][i]["imgUrl"] +"' alt='imgage'>"
                 + "<h3 class='searchable main-h'>" + data["article"][i]["header"] + "</h3>"
                 + "<p class='searchable main-p'>" + data["article"][i]["paragraph"] + "</p>";
            }
            else if(data["article"][i]["type"] === "article") {
                articleContainer.innerHTML += "<div class='card'>" 
                 + "<img class='card-img' src='" + data["article"][i]["imgUrl"] +"' alt='imgage'>"
                 + "<p class='searchable card-p'>" + data["article"][i]["paragraph"] + "</p>"
                 + "</div>";
            }
            else {
                articleBigContainer.innerHTML += "<div class='card'>"
                 + "<img class='card-img' src='" + data["article"][i]["imgUrl"] +"' alt='imgage'>"
                 + "<p class='searchable card-p'>" + data["article"][i]["paragraph"] + "</p>"
                 + "</div>";
            }
        }
    });
}

fetchNews();
fetchArticles();
